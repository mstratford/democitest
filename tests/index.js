const app = require('../app')
const request = require('supertest');

exports.test_index = () => {
  // This test makes a request for the index page (/) and checks it includes the welcome message.
  request(app)
    .get('/')
    .expect('Content-Type', /html/)
    .expect(200)
    .expect(function (res)
      {
        if (!res.text.includes('Welcome to Express')) {
          throw new Error("Missing welcome message on index.")
        }
      }
    )
    .end(function (err, res) {
      if (err) throw err;
    });
}
