const app = require('../app')
const request = require('supertest');

exports.test_users = () => {
  // This test makes a request for the users endpoint (/users) and checks it includes the test JSON message.
  request(app)
    .get('/users')
    .expect('Content-Type', /json/)
    .expect(200)
    .expect(function(res) {
      res.body.message = 'Test Message';
    })
    .end(function (err, res) {
      if (err) throw err;
    });
}
