index = require('./index');
users = require('./users');
const { argv } = require('yargs')

// Allow CI to select a test selection through npm test
function test_selector(name) {
  switch(name) {
    case "index":
      console.log("Running index tests only.")
      index.test_index();
      break;
    case "users":
      console.log("Running users tests only.")
      users.test_users();
      break;
    default:
      console.log("Running all tests.")
      index.test_index();
      users.test_users();
  };
};

test_selector(argv.test_name);
