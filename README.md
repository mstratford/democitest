# DemoCITest

A demonstration of GitLab CI pipelines on an Express project, used for a technical job interview test.

## Manual Install & Running

To install:
`$ npm install --also=dev`

To run the app:
`$ DEBUG=democitest:* npm start`
